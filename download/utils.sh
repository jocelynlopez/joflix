#!/usr/bin/env bash

function build(){
    # Build local directories and files
    touch "$JOFLIX_ALREADY_DOWNLOADED"
    if [ ! -f "$JOFLIX_TRACKED_SERIES" ];then
        echo "*" >> "$JOFLIX_TRACKED_SERIES"
    fi
    mkdir -p "$JOFLIX_DOWNLOAD_SERIES" "$JOFLIX_DOWNLOAD_FILMS"
    mkdir -p "$JOFLIX_ARCHIVED_SERIES" "$JOFLIX_ARCHIVED_FILMS"
}

function update_downloaded_files(){
    # Save previous list
    mv "$JOFLIX_ALREADY_DOWNLOADED" "$JOFLIX_ALREADY_DOWNLOADED.old"

    # Save currently downloaded files
    find "$1" -type f -printf '%f\n' > "$JOFLIX_ALREADY_DOWNLOADED"

    # Delete all duplicate lines
    cp "$JOFLIX_ALREADY_DOWNLOADED" "$JOFLIX_ALREADY_DOWNLOADED.2"
    awk '!a[$0]++' "$JOFLIX_ALREADY_DOWNLOADED.2" > "$JOFLIX_ALREADY_DOWNLOADED"

    # Escape the wild cards ('*', '?', '[', ']' )
    sed -i 's/\[/\\\[/g' "$JOFLIX_ALREADY_DOWNLOADED"
    sed -i 's/\]/\\\]/g' "$JOFLIX_ALREADY_DOWNLOADED"
    sed -i 's/\?/\\\?/g' "$JOFLIX_ALREADY_DOWNLOADED"
    sed -i 's/\*/\\\*/g' "$JOFLIX_ALREADY_DOWNLOADED"

    # Handle file inside directories
    sed -i 's/^/**/' "$JOFLIX_ALREADY_DOWNLOADED"

    # Concatenate old list with current list and delete duplicate lines
    cat "$JOFLIX_ALREADY_DOWNLOADED.old" >> "$JOFLIX_ALREADY_DOWNLOADED"
    cp "$JOFLIX_ALREADY_DOWNLOADED" "$JOFLIX_ALREADY_DOWNLOADED.2"
    awk '!a[$0]++' "$JOFLIX_ALREADY_DOWNLOADED.2" > "$JOFLIX_ALREADY_DOWNLOADED"

    # Cleaning temporary files
    rm "$JOFLIX_ALREADY_DOWNLOADED.old" "$JOFLIX_ALREADY_DOWNLOADED.2"
}

function TEST_update_downloaded_files(){
    # Set all variables
    FILENAME="$1"
    EXPECTED_STRING="$2"
    JOFLIX_DOWNLOAD_DIR="/tmp/test_joflix"
    JOFLIX_FILES_DIR="$JOFLIX_DOWNLOAD_DIR/files"
    JOFLIX_ALREADY_DOWNLOADED="$JOFLIX_DOWNLOAD_DIR/files_already_downloaded.txt"
    FILE="$JOFLIX_FILES_DIR/$FILENAME"

    # Set environment
    rm -rf "$JOFLIX_DOWNLOAD_DIR"
    mkdir -p "$JOFLIX_DOWNLOAD_DIR/files"
    touch "$JOFLIX_ALREADY_DOWNLOADED"
    touch "$FILE"

    # Run function
    update_downloaded_files "$JOFLIX_DOWNLOAD_DIR/files"

    # Verification
    RESULT_STRING=$(cat $JOFLIX_ALREADY_DOWNLOADED)
    if [ "$RESULT_STRING" = "$EXPECTED_STRING" ];then
        echo "Correctly escape : \"$FILENAME\" --> \"$RESULT_STRING\""
        exit 0
    else
        echo "Wrong escape : \"$FILENAME\" --> \"$RESULT_STRING\""
        exit 1
    fi
}
