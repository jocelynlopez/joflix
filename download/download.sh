#!/usr/bin/env bash

source utils.sh

# Load configuration:
# -------------------
if [ -f "$1" ];then
    source "$1"
    build
else
    if [ "$1" = "--build-only" ];then
        build
        exit 0
    else
        echo "No configuration detected."
        echo "You need to give a configuration file (see test.cfg) in first argument"
        exit 1
    fi
fi

# Handle lock file before running rsync and filebot:
# --------------------------------------------------
source lockfile.sh

# Download TV shows:
#-------------------
for index in ${!JOFLIX_SERIES[*]};
do
    SERIE="${JOFLIX_SERIES[$index]}"
    PORT="${JOFLIX_SERIES_PORTS[$index]}"
    OWNER="${JOFLIX_SERIES_OWNER[$index]}"

    rsync -azh --stats --log-file="$JOFLIX_DOWNLOAD_LOG" --usermap="*:$OWNER" --groupmap="*:$OWNER" --chmod=775\
          --exclude-from="$JOFLIX_ALREADY_DOWNLOADED" --include-from="$JOFLIX_TRACKED_SERIES" --exclude="*" \
          -e "ssh -p $PORT" "$SERIE" "$JOFLIX_DOWNLOAD_SERIES"

    update_downloaded_files $JOFLIX_DOWNLOAD_SERIES

    filebot -script fn:amc --conflict auto --action move --def unsorted=y --lang=fr \
            --output "$JOFLIX_ARCHIVED_SERIES" -non-strict "$JOFLIX_DOWNLOAD_SERIES" \
            --log-file "$JOFLIX_FILEBOT_LOG" --def seriesFormat="{n}/{episode.special ? 'Special' : 'Saison '+s.pad(2)}/{n} - {episode.special ? 'S00E'+special.pad(2) : s00e00} - {t}{'.'+lang}" --def unsortedFormat="../unsorted_series/{file.structurePathTail}" \
            --def subtitles=fr,en --def artwork=y --def extras=y --def clean=y
done

# Download movies:
#-----------------
for index in ${!JOFLIX_FILMS[*]};
do
    FILM="${JOFLIX_FILMS[$index]}"
    PORT="${JOFLIX_FILMS_PORTS[$index]}"
    OWNER="${JOFLIX_FILMS_OWNER[$index]}"

    rsync -azh --stats --log-file="$JOFLIX_DOWNLOAD_LOG" --usermap="*:$OWNER" --groupmap="*:$OWNER" --chmod=775 \
          --exclude-from="$JOFLIX_ALREADY_DOWNLOADED" \
          -e "ssh -p $PORT" "$FILM" "$JOFLIX_DOWNLOAD_FILMS"

    update_downloaded_files $JOFLIX_DOWNLOAD_FILMS

    filebot -script fn:amc --conflict auto --action move --def unsorted=y --lang=fr \
            --output "$JOFLIX_ARCHIVED_FILMS" -non-strict "$JOFLIX_DOWNLOAD_FILMS" \
            --log-file "$JOFLIX_FILEBOT_LOG" --def movieFormat="{n} ({y})/{n} ({y}){' CD'+pi}{'.'+lang}" --def unsortedFormat="../unsorted_films/{file.structurePathTail}" \
            --def subtitles=fr,en --def artwork=y --def extras=y --def clean=y
done

# Download other source than tv shows and movies:
#------------------------------------------------
for index in ${!JOFLIX_OTHERS[*]};
do
    OTHER="${JOFLIX_OTHERS[$index]}"
    OTHER=($OTHER)
    SRC=${OTHER[0]}
    DEST=${OTHER[1]}
    PORT="${JOFLIX_OTHERS_PORTS[$index]}"
    OWNER="${JOFLIX_OTHERS_OWNER[$index]}"

    rsync -azh --stats --log-file="$JOFLIX_DOWNLOAD_LOG" --usermap="*:$OWNER" --groupmap="*:$OWNER" --chmod=775 \
          --exclude-from="$JOFLIX_ALREADY_DOWNLOADED" \
          -e "ssh -p $PORT" "$SRC" "$DEST"

    update_downloaded_files $DEST
done

# Cleaning:
# ---------
find "$JOFLIX_DOWNLOAD_SERIES" -type d -empty -delete
find "$JOFLIX_DOWNLOAD_FILMS" -type d -empty -delete
