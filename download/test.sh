#!/usr/bin/env bash

source utils.sh

# -----------------------------------------------------------------------------
#                                     TEST :
#                           update_downloaded_files
# -----------------------------------------------------------------------------
TEST_update_downloaded_files "La vérité si je mens 3 ?.1080p.Fr.Light-BadMad.mkv" "**La vérité si je mens 3 \?.1080p.Fr.Light-BadMad.mkv"
TEST_update_downloaded_files "Resident Evil Apocalypse (2004) [1080p] VFF Dts Hdma Hdlight x264-Ju.mkv" "**Resident Evil Apocalypse (2004) \[1080p\] VFF Dts Hdma Hdlight x264-Ju.mkv"
TEST_update_downloaded_files "Riddick 2013 EXTENDED** 1080p HDRip x264 AC3 -[FAB].mkv" "**Riddick 2013 EXTENDED\*\* 1080p HDRip x264 AC3 -\[FAB\].mkv"


#"La vérité si je mens!3.1080p.Fr.Light-BadMad.mkv"
#"Pirates des Caraïbes 1 2003 Multi-VF2 1080p HDlight.x264~Tonyk~(La Malédiction du Black Pearl).mkv"
#"Resident Evil Apocalypse (2004) [1080p] VFF Dts Hdma Hdlight x264-Ju.mkv"
#"Riddick 2013 EXTENDED 1080p HDRip x264 AC3 -[FAB].mkv"
#"Star Trek Into Darkness (2013) MULTi VF2 [1080p] BluRay x264-PopHD.mkv"
#"Taken 2008 MULTi BluRay 1080p TrueHD 5.1 x265-HD Workshop"
#"The Revenant 2015 MULTi VFF AC3-DTS 1080p HDLight x264.GHT (Le Revenant).mkv"
#"The.Walking.Dead.S07E05.720p.HDTV.x264-FLEET[PRiME]"
#"Transformers 3 [1080p] MULTi 2011 BluRay x264-Pop (dark of the moon) .mkv"
#"Transformers.4.L'Âge.de.L'Extinction.2014.MULTi.HDLight.1080p.AC3.x265-Hom3r.mkv"
#"Twilight 4 2011 Multi-VF2 1080p HDlight.x264~Tonyk~.mkv"

## -----------------------------------------------------------------------------
##                                     TEST 1 :
##                      Only tracked series are downloaded
## -----------------------------------------------------------------------------
## Build fake contents:
#rm -rf "/tmp/test_joflix_download"
#mkdir -p "/tmp/test_joflix_download/remote/series/Smallville.S03E08" "/tmp/test_joflix_download/remote/films"
#SERIES_1="/tmp/test_joflix_download/remote/series/Smallville.S06E01.mkv"
#SERIES_2="/tmp/test_joflix_download/remote/series/The.Walking.Dead.S05E01.mkv"
#SERIES_3="/tmp/test_joflix_download/remote/series/Smallville.S03E08/Smallville.S03E08.mkv"
#FILMS_1="/tmp/test_joflix_download/remote/films/Fight.Club.mkv"
#FILMS_2="/tmp/test_joflix_download/remote/films/Hacksaw.Ridge.(2016).mkv"
#touch "$SERIES_1" && fallocate -l 60M "$SERIES_1"
#touch "$SERIES_2" && fallocate -l 60M "$SERIES_2"
#touch "$SERIES_3" && fallocate -l 60M "$SERIES_3"
#touch "$FILMS_1" && fallocate -l 70M "$FILMS_1"
#touch "$FILMS_2" && fallocate -l 70M "$FILMS_2"
#mkdir -p "/tmp/test_joflix_download/local" && echo "*[sS]mallville*" >> "/tmp/test_joflix_download/local/tracked_series.txt"
#bash ./download.sh test.cfg
#if [ "Smallville" = "`ls /tmp/test_joflix_download/archived/series/`" ];then
#    echo "-----------------------------------------------------------"
#    echo "------- TEST 1 : Only tracked series are downloaded -------"
#    echo "-----------------------------------------------------------"
#    echo "SUCCESS"
#    echo "-----------------------------------------------------------"
#    TEST_1="SUCCESS"
#else
#    echo "-----------------------------------------------------------"
#    echo "------- TEST 1 : Only tracked series are downloaded -------"
#    echo "-----------------------------------------------------------"
#    echo "ERROR : Tracked file are not correctly handled !"
#    echo "Target series downloaded and archived are (Should be only Smallville):"
#    echo "`ls /tmp/test_joflix_download/archived/series/`"
#    echo "-----------------------------------------------------------"
#    TEST_1="ERROR"
#fi

# -----------------------------------------------------------------------------
#                                     TEST 2 :
#       Missing (local) already downloaded files are not downloaded again
# -----------------------------------------------------------------------------
# Build fake contents:
rm -rf "/tmp/test_joflix_download"
mkdir -p "/tmp/test_joflix_download/remote/series/Smallville.S03E08" "/tmp/test_joflix_download/remote/films"
FILMS_1="/tmp/test_joflix_download/remote/films/La vérité si je mens!3.1080p.Fr.Light-BadMad.mkv"
touch "$FILMS_1" && fallocate -l 2G "$FILMS_1"
mkdir -p "/tmp/test_joflix_download/local"
#echo "La vérité si je mens!3.1080p.Fr.Light-BadMad.mkv" >> "/tmp/test_joflix_download/local/files_already_downloaded.txt"
cp "/home/jocelyn/Bureau/files_already_downloaded.txt" "/tmp/test_joflix_download/local/files_already_downloaded.txt"
bash ./download.sh test.cfg
#if [ ! -d "" -a ! -d "" ];then
#    echo "------------------------------------------------------------------------------------------"
#    echo "------- TEST 2 : Missing (local) already downloaded files are not downloaded again -------"
#    echo "------------------------------------------------------------------------------------------"
#    echo "SUCCESS"
#    echo "------------------------------------------------------------------------------------------"
#    TEST_2="SUCCESS"
#else
#    echo "------------------------------------------------------------------------------------------"
#    echo "------- TEST 2 : Missing (local) already downloaded files are not downloaded again -------"
#    echo "------------------------------------------------------------------------------------------"
#    echo "ERROR : Some files have been downloaded !"
#    echo "Target tv shows downloaded are (Should be empty):"
#    echo "`ls "" `"
#    echo "Target movies downloaded are (Should be empty):"
#    echo "`ls "" `"
#    echo "------------------------------------------------------------------------------------------"
#    TEST_2="ERROR"
#fi

## -----------------------------------------------------------------------------
##                                    TEST 3 :
##                    Check if all file are correctly process
## -----------------------------------------------------------------------------
#rm -rf "$JOFLIX_SOURCE_SERIES/*" "$JOFLIX_SOURCE_FILMS/*" "$JOFLIX_DOWNLOAD_FILMS/*" "$JOFLIX_DOWNLOAD_SERIES/*" "$JOFLIX_ARCHIVED_FILMS/*" "$JOFLIX_ARCHIVED_SERIES/*"
#touch "$SERIES_1" && fallocate -l 60M "$SERIES_1"
#touch "$SERIES_2" && fallocate -l 60M "$SERIES_2"
#touch "$SERIES_3" && fallocate -l 60M "$SERIES_3"
#touch "$FILMS_1" && fallocate -l 60M "$FILMS_1"
#touch "$FILMS_2" && fallocate -l 60M "$FILMS_2"
#
#bash ./download.sh test --with-post
#exit 0
#if [ ! -d "$JOFLIX_DOWNLOAD_SERIES" -a ! -d "$JOFLIX_DOWNLOAD_FILMS" ];then
#    echo "------------------------------------------------------------------------------------------"
#    echo "------- TEST 3 : Missing (local) already downloaded files are not downloaded again -------"
#    echo "------------------------------------------------------------------------------------------"
#    echo "SUCCESS"
#    echo "------------------------------------------------------------------------------------------"
#    TEST_2="SUCCESS"
#else
#    echo "------------------------------------------------------------------------------------------"
#    echo "------- TEST 3 : Missing (local) already downloaded files are not downloaded again -------"
#    echo "------------------------------------------------------------------------------------------"
#    echo "ERROR : Some files have been downloaded !"
#    echo "Target tv shows downloaded are (Should be empty):"
#    echo "`ls "$JOFLIX_DOWNLOAD_SERIES" `"
#    echo "Target movies downloaded are (Should be empty):"
#    echo "`ls "$JOFLIX_DOWNLOAD_FILMS" `"
#    echo "------------------------------------------------------------------------------------------"
#    TEST_2="ERROR"
#fi
#
## Cleaning datas
##rm -rf "$TEST_DIR_FOR_JOFLIX"
