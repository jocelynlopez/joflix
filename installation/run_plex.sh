MEDIA="/media/jocelyn/MEDIA"
FILMS="/media/jocelyn/FILMS"
SERIES="/media/jocelyn/SERIES"
TVDB_CURRENT="$MEDIA/videos/series"
TVDB_ARCHIVED="$SERIES/videos/series"
MOVIEDB_CURRENT="$MEDIA/videos/films"
MOVIEDB_ARCHIVED="$FILMS/videos/films"
ANIMEDB="$FILMS/videos/animations"
MUSICDB="$MEDIA/musics"
PHOTODB="$MEDIA/photos"
TRANSCODE="$MEDIA/videos/plex-transcode"
SYNC="$MEDIA/videos/plex-sync"
PLEXCONFIG="/root/configs/plex"

adduser --no-create-home --disabled-password --ingroup media plex
docker pull linuxserver/plex
mkdir -p $PLEXCONFIG

docker run --name="plex" --restart=always --net="host" -d -e TZ="Europe/Paris" \
-e PUID=$(id -u plex) -e GUID=$(id -g plex) \
-e VERSION=latest \
-v $PLEXCONFIG:/config \
-v $TVDB_ARCHIVED:/media/series_archived \
-v $TVDB_CURRENT:/media/series \
-v $MOVIEDB_ARCHIVED:/media/films_archived \
-v $MOVIEDB_CURRENT:/media/films \
-v $MUSICDB:/media/musics \
-v $ANIMEDB:/media/animations \
-v $PHOTODB:/media/photos \
-v $TRANSCODE:/transcode \
-v $SYNC:/sync \
linuxserver/plex
