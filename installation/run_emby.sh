MEDIA="/media/jocelyn/MEDIA"
FILMS="/media/jocelyn/FILMS"
SERIES="/media/jocelyn/SERIES"
DOWNLOADS="/media/jocelyn/DATA/downloads/complete"
TVDB_CURRENT="$MEDIA/videos/series"
TVDB_ARCHIVED="$SERIES/videos/series"
MOVIEDB_CURRENT="$MEDIA/videos/films"
MOVIEDB_ARCHIVED="$FILMS/videos/films"
ANIMEDB="$FILMS/videos/animations"
MUSICDB="$MEDIA/musics"
PHOTODB="$MEDIA/photos"
BOOKSDB="/media/jocelyn/DATA/ebooks"
SYNC="$MEDIA/videos/emby-sync"
TRANSCODE="$MEDIA/videos/emby-transcode"
MOVIE_DOWNLOADS="$DOWNLOADS/films"
TV_DOWNLOADS="$DOWNLOADS/series"
MUSIC_DOWNLOADS="$DOWNLAODS/musics"

EMBYCONFIG="/root/configs/emby"
EMBYIP=8096

adduser --no-create-home --disabled-password --ingroup media emby
docker pull dperson/emby
mkdir -p $EMBYCONFIG

docker run --name="emby" --restart=always --net="host" -d -e TZ="Europe/Paris" \
-e USERID=$(id -u emby) -e GROUPID=$(id -g emby) \
-v $EMBYCONFIG:/config \
-v $TVDB_ARCHIVED:/media/series_archived \
-v $TVDB_CURRENT:/media/series \
-v $MOVIEDB_ARCHIVED:/media/films_archived \
-v $MOVIEDB_CURRENT:/media/films \
-v $MUSICDB:/media/musics \
-v $ANIMEDB:/media/animations \
-v $PHOTODB:/media/photos \
-v $BOOKSDB:/media/books \
-v $SYNC:/media/sync \
-v $TRANSCODE:/media/transcode \
-v $MOVIE_DOWNLOADS:/media/downloads/films \
-v $TV_DOWNLOADS:/media/downloads/series \
-v $MUSIC_DOWNLOADS:/media/downloads/musics \
dperson/emby
