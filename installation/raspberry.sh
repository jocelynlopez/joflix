#!/usr/bin/env bash

install_required()
{
    type $1 >/dev/null 2>&1;
    if [ $? -eq 0 ]; then
        echo "$1 is installed"
    else
        echo "$1 is missing ! Trying to installed it..."
        if [ "$1" = "filebot" ];then
            sudo add-apt-repository -y ppa:webupd8team/java
            sudo apt-get update
            sudo apt-get install oracle-java8-installer -y
            sudo apt-get install oracle-java8-set-default -y
            cd /tmp
            wget http://downloads.sourceforge.net/project/filebot/filebot/FileBot_4.7.2/filebot_4.7.2_amd64.deb?r=http%3A%2F%2Fwww.filebot.net%2F&ts=1479454316&use_mirror=kent
            sudo dpkg -i filebot*
        elif [ "$1" = "docker" ];then
            sudo apt-get install docker.io -y
        else
            sudo apt-get install $1 -y
        fi
    fi
}

function build_emby_container(){

    # Fixed variables
    IMAGE="dperson/emby"
    VOLUMES=""

    # Parse arguments and set input variables
    for i in "$@"
    do
    case $i in
        -u=*|--user=*)
        OWNER="${i#*=}"
        shift # past argument=value
        ;;
        -n=*|--name=*)
        NAME="${i#*=}"
        shift # past argument=value
        ;;
        -c=*|--config=*)
        CONFIG_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $CONFIG_DIR:/config"
        shift # past argument with no value
        ;;
        --sync=*)
        SYNC_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $SYNC_DIR:/media/sync"
        shift # past argument with no value
        ;;
        --transcode=*)
        TRANSCODE_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $TRANSCODE_DIR:/media/transcode"
        shift # past argument with no value
        ;;
        --tv=*)
        TV_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $TV_DIR:/media/series"
        shift # past argument with no value
        ;;
        --movie=*)
        MOVIE_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $MOVIE_DIR:/media/films"
        shift # past argument with no value
        ;;
        --music=*)
        MUSIC_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $MUSIC_DIR:/media/musics"
        shift # past argument with no value
        ;;
        --photo=*)
        PHOTO_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $PHOTO_DIR:/media/photos"
        shift # past argument with no value
        ;;
        --book=*)
        BOOK_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $BOOK_DIR:/media/books"
        shift # past argument with no value
        ;;
        *)
        echo "Unknown option ! (${i#*=})"
        exit 1
        ;;
    esac
    done

    echo "--> Creation des repertoires de '$NAME' si inexistant"
    mkdir -p $CONFIG_DIR && chown -R $OWNER:$OWNER $CONFIG_DIR

    echo "--> Mise à jour de la dernière version de emby"
    docker pull $IMAGE

    echo "--> Suppression du container '$NAME' si existant"
    docker rm -f $NAME

    echo "--> Création de '$NAME' en mode daemon"
    docker run -d --restart=always --name="$NAME" --net="host" \
    -e USERID=$(id -u $OWNER) -e GROUPID=$(id -g $OWNER) \
    -e TZ="Europe/Paris" \
    $VOLUMES \
    $IMAGE
}

function add_an_auto_mount_volume(){
    sudo fdisk -l
    sudo blkid
    echo ""
    echo "Give some informations about the volume to mount automatically"
    read -p "UUID (see previous lines) : " UUID
    read -p "MOUNT POINT (/media/HDD1) : " MOUNT_POINT
    read -p "TYPE (ext4, ntfs, etc.) : " TYPE

    FSTAB_LINE="UUID=$UUID    $MOUNT_POINT           $TYPE    defaults        0       2"
    echo ""
    echo "$FSTAB_LINE"
    read -p "Do you want to add the previous line to /etc/fstab ? (y/n)" response
    if [ "$response" = "y" ];then
        sudo echo "$FSTAB_LINE" >> /etc/fstab
        echo "Line added to /etc/fstab !"
    else
        echo "Adding line to /etc/fstab aborted !"
    fi

}
 -----------------------------------------------------------------------------

# Install prerequisites
install_required flock
install_required rsync
install_required find
install_required awk
install_required filebot
install_required mediainfo
install_required docker

# Configure opensubtitles for filebot:
filebot -script fn:configure

# See your OpenSubtitles download limit:
filebot -script fn:osdb.statslinux.sh

# Automatic mount of external HDD
add_an_auto_mount_volume

# Emby installation
build_emby_container --name=emby --owner=sacha \
                     --config=/opt/configs/emby \
                     --tv=/media/HDD1/series \
                     --movie=/media/HDD1/films \
                     --music=/media/HDD1/musics \
                     --photo=/media/HDD1/photos \
                     --book=/media/HDD1/books \
                     --sync=/media/HDD1/sync \
                     --transcode=/media/HDD1/transcode

