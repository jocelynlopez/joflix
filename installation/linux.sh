#!/usr/bin/env bash

install_required()
{
    type $1 >/dev/null 2>&1;
    if [ $? -eq 0 ]; then
        echo "$1 is installed"
    else
        echo "$1 is missing ! Trying to installed it..."
        if [ "$1" = "filebot" ];then
            sudo add-apt-repository -y ppa:webupd8team/java
            sudo apt-get update
            sudo apt-get install oracle-java8-installer -y
            sudo apt-get install oracle-java8-set-default -y
            cd /tmp
            wget http://downloads.sourceforge.net/project/filebot/filebot/FileBot_4.7.2/filebot_4.7.2_amd64.deb?r=http%3A%2F%2Fwww.filebot.net%2F&ts=1479454316&use_mirror=kent
            sudo dpkg -i filebot*
        else
            sudo apt-get install $1 -y
        fi
    fi
}

install_required flock
install_required rsync
install_required find
install_required awk
install_required filebot
install_required mediainfo

# Configure opensubtitles for filebot:
filebot -script fn:configure

# See your OpenSubtitles download limit:
filebot -script fn:osdb.statslinux.sh