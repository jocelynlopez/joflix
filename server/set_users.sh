#!/usr/bin/env bash

function add_seedbox_user(){
    # $1 : username
    # $2 : group name number 1
    # $3 : group name number 2..etc
    echo "--> Creation de l'utilisateur $1"
    useradd -d /home/"$1" -m "$1"
    passwd "$1"
    usermod -a -G $2 $3 $4 $5 $1
    chown -R "$1":"$1" /home/"$1"
    chmod 770 -R /home/"$1"
    su "$1" -c "ssh-keygen"
    cat "/home/$1/.ssh/id_rsa.pub" >> /home/"$1"/.ssh/authorized_keys
    chsh -s /bin/bash "$1"
}

echo "Création des groupes d'utilisateur"
groupadd sshusers && echo "AllowGroups sshusers" >> /etc/ssh/sshd_config
if [ -z $(grep "AllowGroups sshusers" /etc/ssh/sshd_config) ];then
    echo "AllowGroups sshusers" >> /etc/ssh/sshd_config
fi

echo "--> Création de l'utilisateur commun"
add_seedbox_user "commun"

echo "--> Création de(s) utilisateur(s) administrateur"
add_seedbox_user "jocelyn" commun sudo

echo "--> Création de(s) utilisateur(s) classique"
add_seedbox_user jeremy commun
add_seedbox_user lloven commun
add_seedbox_user bilel commun
add_seedbox_user sacha commun
add_seedbox_user maxime commun