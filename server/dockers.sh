#!/usr/bin/env bash


function build_transmission_container(){

    # Fixed variables
    IMAGE="linuxserver/transmission"
    VOLUMES=""

    # Parse arguments and set input variables
    for i in "$@"
    do
    case $i in
        -t=*|--type=*)
        TYPE="${i#*=}"
        shift # past argument=value
        ;;
        -u=*|--user=*)
        OWNER="${i#*=}"
        shift # past argument=value
        ;;
        -n=*|--name=*)
        NAME="${i#*=}"
        shift # past argument=value
        ;;
        --passwd=*)
        PASSWD="${i#*=}"
        shift # past argument=value
        ;;
        -p=*|--port=*)
        PORTS="${i#*=}"
        shift # past argument with no value
        ;;
        -c=*|--config=*)
        CONFIG_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $CONFIG_DIR:/config"
        shift # past argument with no value
        ;;
        --dl-dir=*)
        DOWNLOAD_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $DOWNLOAD_DIR:/downloads/complete"
        shift # past argument with no value
        ;;
        *)
        echo "Unknown option ! (${i#*=})"
        exit 1
        ;;
    esac
    done

    PORT_DL=${PORTS%,*}
    PORT_UL=${PORTS##*,}

    echo "--> Creation des repertoires du container '$NAME' si inexistant"
    mkdir -p $DOWNLOAD_DIR && chown -R $OWNER:$OWNER $DOWNLOAD_DIR
    mkdir -p $CONFIG_DIR && chown -R $OWNER:$OWNER $CONFIG_DIR

    echo "--> Mise à jour de la dernière version de transmission"
    docker pull $IMAGE

    echo "--> Suppression de '$NAME' si existant"
    docker rm -f ${NAME}

    echo "--> Création de '$NAME' en mode daemon"
    docker run -d --restart=always --name="$NAME" \
    $VOLUMES \
    -e PGID=$(id -g $OWNER) -e PUID=$(id -u $OWNER) \
    -e TZ="Europe/Paris" \
    -p $PORT_DL:9091 -p $PORT_UL:51413 -p $PORT_UL:51413/udp \
    $IMAGE

    echo "--> Création de l'authentification de '$NAME'"
    sleep 2
    docker stop $NAME
    sleep 2
    sed -i 's/"rpc-authentication-required":.*/"rpc-authentication-required": true,/' $CONFIG_DIR/settings.json
    sed -i "s/\"rpc-username\":.*/\"rpc-username\": \"$OWNER\",/" $CONFIG_DIR/settings.json
    sed -i "s/\"rpc-password\":.*/\"rpc-password\": \"$PASSWD\",/" $CONFIG_DIR/settings.json
    docker start $NAME
}

function build_sickrage_container(){

    # Fixed variables
    IMAGE="linuxserver/sickrage"
    VOLUMES=""

    # Parse arguments and set input variables
    for i in "$@"
    do
    case $i in
        -u=*|--user=*)
        OWNER="${i#*=}"
        shift # past argument=value
        ;;
        -p=*|--port=*)
        PORT="${i#*=}"
        shift # past argument with no value
        ;;
        -n=*|--name=*)
        NAME="${i#*=}"
        shift # past argument=value
        ;;
        --tv-dir=*)
        TV_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $TV_DIR:/tv"
        shift # past argument with no value
        ;;
        --dl-dir=*)
        DOWNLOAD_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $DOWNLOAD_DIR:/downloads"
        shift # past argument with no value
        ;;
        -c=*|--config=*)
        CONFIG_DIR="${i#*=}"
        VOLUMES="$VOLUMES -v $CONFIG_DIR:/config"
        shift # past argument with no value
        ;;
        *)
        echo "Unknown option ! (${i#*=})"
        exit 1
        ;;
    esac
    done

    echo "--> Creation des repertoires de '$NAME' si inexistant"
    mkdir -p $DOWNLOAD_DIR && chown -R $OWNER:$OWNER $DOWNLOAD_DIR
    mkdir -p $TV_DIR && chown -R $OWNER:$OWNER $TV_DIR
    mkdir -p $CONFIG_DIR && chown -R $OWNER:$OWNER $CONFIG_DIR

    echo "--> Mise à jour de la dernière version de sickrage"
    docker pull $IMAGE

    echo "--> Suppression du container '$NAME' si existant"
    docker rm -f $NAME

    echo "--> Création de '$NAME' en mode daemon"
    docker run -d --restart=always --name=$NAME \
    $VOLUMES \
    -e PGID=$(id -g $OWNER) -e PUID=$(id -u $OWNER) \
    -e TZ="Europe/Paris" \
    -p $PORT:8081 \
    $IMAGE
}