#!/usr/bin/env bash

source dockers.sh

# Commun
build_transmission_container --name=commun_TR_films --user=commun --passwd=mdp --port=9001,51401 --config=/home/commun/configs/TRANSMISSION_films --dl-dir=/home/commun/downloads/films
build_transmission_container --name=commun_TR_series --user=commun --passwd=mdp --port=9002,51402 --config=/home/commun/configs/TRANSMISSION_series --dl-dir=/home/commun/downloads/series
build_sickrage_container --name=commun_SICKRAGE --user=commun --port=8001 --config=/home/commun/configs/SICKRAGE

# Jocelyn
build_transmission_container --name=jocelyn_TR --user=jocelyn --passwd=mdp --port=9011,51411 --config=/home/jocelyn/configs/TRANSMISSION --dl-dir=/home/jocelyn/downloads






