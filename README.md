# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How to automatically connect to a server without password prompt ? ###

```bash
# Set your server url:
SERVER="my_server_url.com"

# Create your ssh id if it doesn't exist (set a blank pass phrase):
if [ ! -f ~/.ssh/id_rsa.pub ];then
    ssh-keygen -t rsa
fi

# Send your id to the server
ssh-copy-id $USER@$SERVER
```

### How to run this script ? ###
```bash
# Edit your configuration (use configuration from test.cfg)

# Run the script with your configuration
cd directory_where_download.sh_is
bash ./download.sh my_configuration.cfg
```